import {ProductStatus} from "./product-status";

export class Product {

  id: number;
  name: string;
  description: string;
  price: number;
  currency: string;
  status: ProductStatus;

  constructor(id: number,
              name: string,
              description: string,
              price: number,
              currency: string,
              status: ProductStatus) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.currency = currency;
    this.status = status;
  }
}
