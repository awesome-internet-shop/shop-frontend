export enum ProductStatus {

  ACTIVE, BLOCKED, DELETED
}
