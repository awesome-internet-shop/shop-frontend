import {ProfileStatus} from "./profile-status";

export class Profile {

  surname: string;
  name: string;
  birthDate: Date;
  status: ProfileStatus

  constructor(surname: string, name: string, birthDate: Date, status: ProfileStatus) {
    this.surname = surname;
    this.name = name;
    this.birthDate = birthDate;
    this.status = status;
  }
}
