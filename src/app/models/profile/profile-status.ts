export enum ProfileStatus {

  ACTIVE, BLOCKED, DELETED
}
