import {OrderItem} from "./order-item";
import {OrderStatus} from "./order-status";
import {PaymentMethod} from "./payment-method";
import {DeliveryMethod} from "./delivery-method";

export class Order {

  id: number;
  profileId: number;
  deliveryMethod: DeliveryMethod;
  paymentMethod: PaymentMethod;
  status: OrderStatus;
  items: OrderItem[];

  constructor(id: number,
              profileId: number,
              deliveryMethod: DeliveryMethod,
              paymentMethod: PaymentMethod,
              status: OrderStatus,
              items: OrderItem[]) {
    this.id = id;
    this.profileId = profileId;
    this.deliveryMethod = deliveryMethod;
    this.paymentMethod = paymentMethod;
    this.status = status;
    this.items = items;
  }
}
