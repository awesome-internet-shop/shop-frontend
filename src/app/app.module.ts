import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppComponent} from "./app.component";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule, RouterOutlet} from "@angular/router";
import {ComponentsModule} from "./components/components.module";
import {ServicesModule} from "./services/services.module";
import {AppRoutingModule} from "./app.routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    RouterOutlet,
    AppRoutingModule,
    ComponentsModule,
    ServicesModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
