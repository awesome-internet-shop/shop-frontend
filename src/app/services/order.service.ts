import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {OrderItemCreationRequest} from "../models/order/order-item-creation-request";
import {Order} from "../models/order/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {
  }

  getCurrentShoppingCartItemsCount(): Observable<number> {
    return this.http.get<number>(
      `${environment.orderBaseUrl}/shopping-cart/items/count`,
      { headers: new HttpHeaders({ 'profileId': 123 }) }
    );
  }

  getCurrentShoppingCartOrder(): Observable<Order> {
    return this.http.get<Order>(
      `${environment.orderBaseUrl}/shopping-cart`,
      { headers: new HttpHeaders({ 'profileId': 123 }) }
    );
  }

  addShoppingCartItem(orderItemCreationRequest: OrderItemCreationRequest): Observable<void> {
    return this.http.post<void>(
      `${environment.orderBaseUrl}/shopping-cart/items`,
      orderItemCreationRequest,
      { headers: new HttpHeaders({ 'profileId': 123 }) }
    );
  }
}
