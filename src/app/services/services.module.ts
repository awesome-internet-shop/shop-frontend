import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {ProfileService} from "./profile.service";
import {ProductService} from "./product.service";
import {OrderService} from "./order.service";


@NgModule({
  providers: [ProfileService, ProductService, OrderService],
  imports: [
    HttpClientModule
  ]
})
export class ServicesModule {
}
