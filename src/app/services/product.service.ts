import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Product} from "../models/product/product";
import {environment} from "../../environments/environment";
import {Page} from "../models/shared/page";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {
  }

  getAllProducts(currentPage: number, pageSize: number): Observable<Page<Product>> {
    let params = { pageNumber: currentPage, pageSize: pageSize };
    return this.http.get<Page<Product>>(
      `${environment.productBaseUrl}/products`,
      { params: params }
    );
  }
}
