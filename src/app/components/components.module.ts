import {NgModule} from '@angular/core';
import {HomeComponent} from "./home/home.component";
import {CommonModule} from "@angular/common";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatPaginatorModule} from "@angular/material/paginator";
import {AuthorizationComponent} from "./authorization/authorization.component";
import {MatCardModule} from "@angular/material/card";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {MatButtonModule} from "@angular/material/button";
import {ProductsComponent} from "./products/products.component";
import {HeaderComponent} from "./header/header.component";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {RouterLink} from "@angular/router";
import {MatBadgeModule} from "@angular/material/badge";


@NgModule({
  imports: [
    CommonModule,
    ScrollingModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    RouterLink,
    MatBadgeModule
  ],
  declarations: [AuthorizationComponent, HeaderComponent, HomeComponent, ProductsComponent, ShoppingCartComponent],
  exports: [HeaderComponent]
})
export class ComponentsModule {
}
