import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {ProductService} from "../../services/product.service";
import {Product} from "../../models/product/product";
import {OrderService} from "../../services/order.service";
import {OrderItemCreationRequest} from "../../models/order/order-item-creation-request";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.less'
})
export class ProductsComponent implements OnInit, OnDestroy {

  public readonly PRODUCT_CARD_HEIGHT: number = 120;
  private readonly PAGE_SIZE = 10;
  public readonly BUFFER_SIZE: number = 5;
  private readonly UNSUBSCRIBE_NOTIFIER = new Subject();

  public isProductsLoading: boolean = true;
  public products: Product[] = [];

  private isAllProductsLoaded: boolean = false;

  constructor(private productService: ProductService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.loadNextProducts();
  }

  loadNextProducts() {
    if (this.isAllProductsLoaded) {
      console.log('All products are loaded.');
    }
    console.log('Loading next products...');
    const startPageNumber = this.products.length / this.PAGE_SIZE;
    if (!Number.isInteger(startPageNumber)) {
      console.log('Does not load products, number is not integer.');
      return;
    }
    this.isProductsLoading = true;
    this.productService.getAllProducts(startPageNumber, this.PAGE_SIZE)
      .pipe(takeUntil(this.UNSUBSCRIBE_NOTIFIER))
      .subscribe(pageableProducts => {
        console.log(pageableProducts);
        this.products = this.products.concat(pageableProducts.content);
        this.isProductsLoading = false;
        this.isAllProductsLoaded = pageableProducts.last;
      });
  }

  onScrollPositionChange(newIndex: number) {
    if (this.isProductsLoading || this.isAllProductsLoaded) {
      return;
    }
    console.log('Scroll position changed, ', newIndex);
    if (newIndex >= this.products.length - this.BUFFER_SIZE) {
      this.loadNextProducts();
    }
  }

  onBuyClick(productId: number, quantity: number) {
    this.orderService.addShoppingCartItem(new OrderItemCreationRequest(productId, quantity))
      .pipe(takeUntil(this.UNSUBSCRIBE_NOTIFIER))
      .subscribe(() => console.log('Product added to shopping cart.'));
  }

  ngOnDestroy() {
    this.UNSUBSCRIBE_NOTIFIER.next(null);
    this.UNSUBSCRIBE_NOTIFIER.complete();
  }
}
