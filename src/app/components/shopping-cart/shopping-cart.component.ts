import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderService} from "../../services/order.service";
import {Subject, takeUntil} from "rxjs";
import {Order} from "../../models/order/order";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrl: './shopping-cart.component.less'
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

  private readonly UNSUBSCRIBE_NOTIFIER = new Subject();

  public currentOrder: Order | undefined;

  constructor(private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orderService.getCurrentShoppingCartOrder()
      .pipe(takeUntil(this.UNSUBSCRIBE_NOTIFIER))
      .subscribe(order => {
        console.log(order);
        this.currentOrder = order;
      });
  }

  ngOnDestroy() {
    this.UNSUBSCRIBE_NOTIFIER.next(null);
    this.UNSUBSCRIBE_NOTIFIER.complete();
  }
}
