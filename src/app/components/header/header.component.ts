import {Component, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {OrderService} from "../../services/order.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.less'
})
export class HeaderComponent implements OnInit {

  public searchControl = new FormControl<string>('');
  public isShoppingCartItemsCountHidden: boolean = true;
  public shoppingCartItemsCount: number = 0;

  constructor(private router: Router,
              private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orderService.getCurrentShoppingCartItemsCount()
      .subscribe(count => {
        if (count !== 0) {
          this.shoppingCartItemsCount = count;
          this.isShoppingCartItemsCountHidden = false;
        }
      });
  }

  navigateToShoppingCart() {
    this.router.navigate(['/shopping-cart'])
      .then();
  }
}
