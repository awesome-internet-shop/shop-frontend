import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AuthorizationComponent} from "./components/authorization/authorization.component";
import {HomeComponent} from "./components/home/home.component";
import {ShoppingCartComponent} from "./components/shopping-cart/shopping-cart.component";

export const routes: Routes = [
  { path: 'login', component: AuthorizationComponent, title: 'Authorization' },
  { path: 'home', component: HomeComponent, title: 'Home Page' },
  { path: 'shopping-cart', component: ShoppingCartComponent, title: 'Shopping cart' },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
